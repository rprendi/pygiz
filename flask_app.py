
# A very simple Flask Hello World app for you to get started with...

from flask import Flask, render_template, request, redirect, url_for
from products import Product
from pony.orm import Database, Required, db_session, PrimaryKey, select, delete

app = Flask(__name__)

db = Database()

class ProductTable(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    price = Required(int)
    quantity = Required(int)


db.bind(provider='sqlite', filename='mydb', create_db=True)
db.generate_mapping(create_tables=True)

#my_products = [Product('Bluze', 20, 50), Product('Jeans', 30, 40), Product('Jacket', 100, 50)]



@app.route('/')
def index():
    return render_template('index.html')

@app.route('/table', methods=['GET', 'POST'])
@db_session
def table():
    title = "Products table"

    if request.method == 'POST':
        search_name = request.form['name']
        search_price = int(request.form['price'])
        search_quantity = int(request.form['quantity'])

        ProductTable(name=search_name, price=search_price, quantity=search_quantity)
        #my_products.append(Product(search_name, search_price,search_quantity))
        return redirect(url_for('table'))
    else:
        search_term = request.args.get('search','')

        if search_term:
            my_products = select(p for p in ProductTable if search_term.lower() in p.name.lower())[:]
            return render_template('table.html', TITLE=title, MY_PRODUCTS=my_products)
        else:
            my_products = ProductTable.select()[:]
            return render_template('table.html', TITLE=title, MY_PRODUCTS=my_products)

@app.route('/delete/<int:id>')
@db_session
def delete_route(id):

    delete(p for p in ProductTable if p.id == id)
    return redirect(url_for('table'))


@app.route('/edit/<int:product_id>', methods=['GET','POST'])
@db_session
def edit_product(product_id):
    product = ProductTable.get(id=product_id)
    if request.method == 'GET':
        return render_template('product.html',PRODUCT=product)
    else:
        changed_name = request.form['name']
        changed_price = int(request.form['price'])
        changed_quantity = int(request.form['quantity'])
        # product_id = int(request.form['id'])
        # product = ProductTable.get(id=product_id)
        product.set(name=changed_name,price=changed_price, quantity=changed_quantity)
        return redirect(url_for('table'))

